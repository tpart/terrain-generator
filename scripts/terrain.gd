extends Spatial
tool


const UNLOAD_DISTANCE_FACTOR: float = 1.5

export var player: NodePath

var load_distance: int = 6
var chunk_size: int = 128
var chunk_resolution: float = 16
var terrain_height: float = 150
var chunks := {}
var chunk_queue := []
var player_node
var terrain_material = preload("res://assets/materials/terrain.tres")
var noise := OpenSimplexNoise.new()

onready var chunks_node = $ChunksNode


func _ready():
	noise.seed = 0
	noise.octaves = 7
	noise.period = 256
	noise.persistence = 0.5
	if player != "" and not Engine.editor_hint:
		player_node = get_node_or_null(player)


func _process(_delta):
	# Add chunks to queue
	var player_pos: Vector3
	if player_node == null:
		player_pos = Vector3.ZERO
	else:
		player_pos = player_node.translation
	for x in range(-load_distance, load_distance):
		for z in range(-load_distance, load_distance):
			var chunk_pos = Vector2(x * chunk_size + int(player_pos.x / chunk_size) * chunk_size, z * chunk_size + int(player_pos.z / chunk_size) * chunk_size)
			if not chunk_pos in chunk_queue and not chunk_pos in chunks.keys():
				chunk_queue.push_back(chunk_pos)
	
	# Start generating chunks
	for i in chunk_queue:
		chunks[i] = null
		chunk_queue.erase(i)
		var surface_tool := SurfaceTool.new()
		var thread := Thread.new()
		thread.start(self, "generate_chunk", [i, surface_tool, thread])
	
	# Unload chunks that are too far away
	if not Engine.editor_hint:
		unload_chunks(player_pos)


func generate_chunk(data: Array) -> void:
	var chunk_pos: Vector2 = data[0]
	var surface_tool: SurfaceTool = data[1]
	var thread: Thread = data[2]
	surface_tool.begin(Mesh.PRIMITIVE_TRIANGLES)
	var square_size: float = chunk_size / chunk_resolution
	var square_count: int = chunk_size / square_size
	for x in range(0, square_count):
		for z in range(0, square_count):
			surface_tool.add_vertex(Vector3(x * square_size, noise.get_noise_2d(x * square_size + chunk_pos.x, z * square_size + chunk_pos.y) * terrain_height, z * square_size))
			surface_tool.add_vertex(Vector3((x + 1) * square_size, noise.get_noise_2d((x + 1) * square_size + chunk_pos.x, z * square_size + chunk_pos.y) * terrain_height, z * square_size))
			surface_tool.add_vertex(Vector3(x * square_size, noise.get_noise_2d(x * square_size + chunk_pos.x, (z + 1) * square_size + chunk_pos.y) * terrain_height, (z + 1) * square_size))
			surface_tool.add_vertex(Vector3(x * square_size, noise.get_noise_2d(x * square_size + chunk_pos.x, (z + 1) * square_size + chunk_pos.y) * terrain_height, (z + 1) * square_size))
			surface_tool.add_vertex(Vector3((x + 1) * square_size, noise.get_noise_2d((x + 1) * square_size + chunk_pos.x, z * square_size + chunk_pos.y) * terrain_height, z * square_size))
			surface_tool.add_vertex(Vector3((x + 1) * square_size, noise.get_noise_2d((x + 1) * square_size + chunk_pos.x, (z + 1) * square_size + chunk_pos.y) * terrain_height, (z + 1) * square_size))
	surface_tool.generate_normals()
	var mesh: ArrayMesh = surface_tool.commit()
	call_deferred("add_chunk", mesh, chunk_pos, thread)


func add_chunk(mesh: ArrayMesh, chunk_pos: Vector2, thread) -> void:
	thread.wait_to_finish()
	var static_body = StaticBody.new()
	static_body.name = "%s,%s" % [str(chunk_pos.x), str(chunk_pos.y)]
	static_body.translation = Vector3(chunk_pos.x, 0, chunk_pos.y)
	var mesh_instance = MeshInstance.new()
	mesh_instance.mesh = mesh
	mesh_instance.material_override = terrain_material
	chunks_node.add_child(static_body)
	static_body.add_child(mesh_instance)
	chunks[chunk_pos] = static_body
	if not Engine.editor_hint:
		mesh_instance.create_trimesh_collision()


func unload_chunks(player_pos: Vector3) -> void:
	var chunk_list := []
	var unload_distance = load_distance * chunk_size * UNLOAD_DISTANCE_FACTOR * 2
	var player_pos_2d: Vector2 = Vector2(player_pos.x, player_pos.y)
	for i in chunks.keys():
		if player_pos_2d.distance_to(i) > unload_distance:
			chunk_list.push_back(i)
	if chunk_list.size() > 0:
		remove_chunks(chunk_list)


func remove_chunks(chunk_array: Array) -> void:
	for i in chunk_array:
		if chunks.keys().has(i):
			if chunks[i] != null:
				chunks[i].queue_free()
			chunks.erase(i)
